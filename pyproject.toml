[tool.poetry]
name = "crosswords"
version = "0.1.0"
description = "Crosswords & Clues public website"
authors = ["Gledi Caushaj <gledi.alb@gmail.com>"]
license = "MPL-2.0"
readme = "README.md"

[tool.poetry.dependencies]
python = "^3.10"
django = "^4.0.7"
djangorestframework = "^3.13.1"
djangorestframework-simplejwt = "^5.2.0"
djangorestframework-camel-case = "^1.3.0"
django-health-check = "^3.16.5"
django-cors-headers = "^3.13.0"
django-crispy-forms = "^1.14.0"
django-filter = "^22.1"
django-redis = "^5.2.0"
django-taggit = "^3.0.0"
drf-spectacular = "^0.23.1"
environs = "^9.5.0"
dj-database-url = "^1.0.0"
cryptography = "^37.0.4"
pillow = "^9.2.0"
psycopg2 = "^2.9.3"
inflection = "^0.5.1"
channels = "^3.0.5"
channels-redis = "^3.4.1"
requests = "^2.28.1"
redis = "^4.3.4"
hiredis = "^2.0.0"
celery = "^5.2.7"
django-celery-results = "^2.4.0"
django-celery-beat = "^2.3.0"

[tool.poetry.group.dev.dependencies]
django-extensions = "^3.2.0"
django-debug-toolbar = "^3.5.0"
ipython = "^8.4.0"
black = "^22.6.0"
flake8 = "^5.0.4"
graphviz = "^0.20.1"
isort = "^5.10.1"

[tool.poetry.group.test.dependencies]
pytest = "^7.1.2"
pytest-django = "^4.5.2"
pytest-cov = "^3.0.0"
coverage = "^6.4.2"
faker = "^13.15.1"

[tool.poetry.group.prod.dependencies]
twisted = {version = "^22.4.0", extras = ["tls", "http2"]}
daphne = "^3.0.2"

[build-system]
requires = ["poetry-core"]
build-backend = "poetry.core.masonry.api"

[tool.pytest.ini_options]
pythonpath = ["crosswords"]
python_files = ["test_*.py", "*_tests.py"]
xfail_strict = true
addopts = [
    "--ds=core.settings.local",
    "--strict-config",
    "--strict-markers",
    "--cov=crosswords",
    "--cov-branch",
    "--cov-report=term-missing"
]

[tool.coverage.run]
omit = [
    "crosswords/tests/*",
    "crosswords/manage.py",
    "crosswords/core/settings/*.py",
    "crosswords/**/__init__.py",
    "crosswords/**/migrations/*.py",
]

[tool.black]
force-exclude = '(migrations\/)'

[tool.isort]
profile = "black"
src_paths = ["crosswords"]
default_section = "THIRDPARTY"
known_first_party = "crosswords"
known_django = "django"
sections = ["FUTURE", "STDLIB", "DJANGO", "THIRDPARTY", "FIRSTPARTY", "LOCALFOLDER"]

[tool.pycln]
all = true
verbose = true
extend_exclude = '(__init__\.py|settings\/.*\.py|migrations\/.*\.py)'
