import os
from typing import Any

from django.conf import settings
from django.db import models
from django.urls import reverse
from django.utils import timezone as timezone
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _

from core.models import BaseModel


class Post(BaseModel):
    title = models.CharField(_("title"), max_length=255, null=False, blank=False)
    slug = models.SlugField(
        _("slug"),
        max_length=255,
        unique=True,
        null=False,
        blank=True,
    )
    content = models.TextField(_("content"), null=False, blank=False)
    is_published = models.BooleanField(
        _("is published"),
        default=False,
        null=False,
        blank=False,
    )
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("author"),
        null=True,
        blank=True,
        related_name="posts",
        on_delete=models.SET_NULL,
    )

    class Meta:
        db_table = "posts"
        verbose_name = _("post")
        verbose_name_plural = _("posts")
        get_latest_by = ("created", "modified")

    def __str__(self) -> str:
        return self.title

    def save(self, *args: Any, **kwargs: Any) -> None:
        if not self.slug:
            self.slug = slugify(self.title)
        self.modified = timezone.now()
        return super().save(*args, **kwargs)

    def get_absolute_url(self) -> str:
        return reverse("blog:post_detail", kwargs={"slug": self.slug})


def _upload_to(instance, filename):
    name, ext = os.path.splitext(filename)
    dt = "{:%Y%m%d%H%M%S}".format(timezone.now())
    new_name = f"{slugify(name)}_{dt}{ext}"
    return f"pictures/{instance.post.key}/{new_name}"


class Picture(BaseModel):
    caption = models.CharField(_("caption"), max_length=255, null=True, blank=True)
    name = models.CharField(_("name"), max_length=255)
    picture = models.ImageField(
        _("picture"),
        null=False,
        blank=False,
        upload_to=_upload_to,
    )
    post = models.ForeignKey(
        Post,
        verbose_name=_("post"),
        null=False,
        blank=False,
        related_name="pictures",
        on_delete=models.CASCADE,
    )
    is_cover = models.BooleanField(_("is cover"), default=False)

    class Meta:
        db_table = "pictures"
        verbose_name = _("picture")
        verbose_name_plural = _("pictures")
        get_latest_by = ("created", "modified")

    def __str__(self) -> str:
        return self.caption if self.caption else self.name

    def save(self, *args: Any, **kwargs: Any) -> None:
        self.modified = timezone.now()
        if not self.name:
            self.name = self.picture.filename
        return super().save(*args, **kwargs)

    def get_absolute_url(self) -> str:
        return reverse("blog:picture_detail", kwargs={"pk": self.pk})
