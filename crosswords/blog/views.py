from django.views.generic import DetailView, ListView

from blog.models import Post


class PostListView(ListView):
    queryset = Post.objects.select_related("author").all()


class PostDetailView(DetailView):
    queryset = Post.objects.select_related("author").all()
