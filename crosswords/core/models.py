import uuid
from typing import Any

from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _


class BaseModel(models.Model):
    id = models.UUIDField(
        _("id"),
        primary_key=True,
        null=False,
        blank=False,
        default=uuid.uuid4,
        editable=False,
    )
    created = models.DateTimeField(_("created"), editable=False, default=timezone.now)
    modified = models.DateTimeField(_("modified"), editable=False, default=timezone.now)

    class Meta:
        abstract = True

    @property
    def key(self) -> str:
        return self.pk.hex

    def save(self, *args: Any, **kwargs: Any) -> None:
        self.modified = timezone.now()
        return super().save(*args, **kwargs)
