from uuid import UUID


class HexUUIDConverter:
    regex = r"[0-9a-f]{32}"

    def to_python(self, value):
        return UUID(hex=value)

    def to_url(self, value):
        return value.hex
