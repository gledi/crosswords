from core.settings.common import *
from core.settings.common import INSTALLED_APPS, MIDDLEWARE, env

INSTALLED_APPS += [
    "debug_toolbar",
    "django_extensions",
]

MIDDLEWARE.insert(2, "debug_toolbar.middleware.DebugToolbarMiddleware")

INTERNAL_IPS = ["127.0.0.1"]
DEBUG_TOOLBAR_CONFIG = {
    "INTERCEPT_REDIRECTS": False,
}

SHELL_PLUS_PRINT_SQL = env.bool("SHELL_PLUS_PRINT_SQL", default=False)
SHELL_PLUS_IMPORTS = [
    "import io",
    "import os",
    "import sys",
    "import csv",
    "import uuid",
    "import random",
    "import secrets",
    "import datetime",
    "import inflection",
    "import requests",
    "from uuid import UUID",
    "from decimal import Decimal",
    "from faker import Faker",
]
