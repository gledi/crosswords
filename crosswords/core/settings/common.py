import warnings
from datetime import timedelta
from pathlib import Path

from environs import Env

env = Env()

_environments = frozenset(["local", "development", "staging", "production"])
ENV = env("ENVIRONMENT", default="local")
if ENV not in _environments:
    _env_names = ", ".join(_environments)
    warnings.warn(f"ENVIRONMENT must be one of {_env_names}")

BASE_DIR = Path(__file__).resolve().parent.parent.parent
ROOT_DIR = BASE_DIR.parent

SECRET_KEY = env("SECRET_KEY", default="SuperSecretKey")

DEBUG = env.bool("DEBUG", default=False)

ALLOWED_HOSTS = env.list("ALLOWED_HOSTS", default=[])


INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.postgres",
    # 3rd party apps
    "taggit",
    "channels",
    "rest_framework",
    "rest_framework.authtoken",
    "corsheaders",
    "drf_spectacular",
    "django_filters",
    "health_check",
    "health_check.db",
    "health_check.cache",
    "django_celery_results",
    "django_celery_beat",
    # local apps
    "pages",
    "users",
    "clues",
    "blog",
    "apiv1",
]


MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "core.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [BASE_DIR / "templates"],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "clues.context_processors.get_publishers",
                "clues.context_processors.get_latest_clues",
                "clues.context_processors.get_random_clues",
            ],
        },
    },
]

WSGI_APPLICATION = "core.wsgi.application"
ASGI_APPLICATION = "core.asgi.application"

# user model
AUTH_USER_MODEL = "users.User"

DATABASES = {"default": env.dj_db_url("DATABASE_URL")}

# cache configuration
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": env.str("CACHE_URL"),
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "PARSER_CLASS": "redis.connection.HiredisParser",
        },
    },
}

# Session backend
SESSION_ENGINE = "django.contrib.sessions.backends.cache"
SESSION_CACHE_ALIAS = "default"

# Channels
CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": env.list(
                "CHANNEL_LAYERS_HOSTS", default=["redis://localhost:6379/0"]
            ),
        },
    }
}


AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",  # noqa: E501
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]


LANGUAGE_CODE = "en-us"
TIME_ZONE = "UTC"
USE_I18N = True
USE_L10N = True
USE_TZ = True


DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"


STATICFILES_DIRS = [BASE_DIR / "static"]

STATIC_URL = "/static/"
STATIC_ROOT = ROOT_DIR / "static"


MEDIA_URL = "/media/"
MEDIA_ROOT = ROOT_DIR / "media"


REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "rest_framework_simplejwt.authentication.JWTAuthentication",
        "rest_framework.authentication.TokenAuthentication",
    ),
    "DEFAULT_PERMISSION_CLASSES": ("rest_framework.permissions.IsAuthenticated",),
    # "DEFAULT_PERMISSION_CLASSES": ("rest_framework.permissions.AllowAny",),
    "DEFAULT_RENDERER_CLASSES": (
        "djangorestframework_camel_case.render.CamelCaseJSONRenderer",
        "djangorestframework_camel_case.render.CamelCaseBrowsableAPIRenderer",
    ),
    "DEFAULT_PARSER_CLASSES": (
        "djangorestframework_camel_case.parser.CamelCaseFormParser",
        "djangorestframework_camel_case.parser.CamelCaseMultiPartParser",
        "djangorestframework_camel_case.parser.CamelCaseJSONParser",
    ),
    "DEFAULT_PAGINATION_CLASS": "core.pagination.DefaultPagination",
    "DEFAULT_FILTER_BACKENDS": ("django_filters.rest_framework.DjangoFilterBackend",),
    "DEFAULT_SCHEMA_CLASS": "drf_spectacular.openapi.AutoSchema",
}

SPECTACULAR_SETTINGS = {
    "TITLE": "Crosswords API",
    "DESCRIPTION": "Crosswords & Clues",
    "VERSION": "0.1.1",
}

# setup CORS
CORS_ALLOWED_ORIGINS = env.list("CORS_ALLOWED_ORIGINS", default=[])
CORS_URLS_REGEX = r"^/api/.*$"

SIMPLE_JWT = {
    "AUTH_HEADER_TYPES": ("Bearer",),
    "SIGNING_KEY": env.str("SECRET_KEY", default="SuperDuperSecret"),
    "ACCESS_TOKEN_LIFETIME": timedelta(minutes=15),
    "REFRESH_TOKEN_LIFETIME": timedelta(weeks=2),
}

SPECTACULAR_SETTINGS = {
    "TITLE": "Crosswords",
    "DESCRIPTION": "Crosswords & Clues",
    "VERSION": "0.1.0",
    "GENERIC_ADDITIONAL_PROPERTIES": "bool",
}


CELERY_TIMEZONE = "UTC"
CELERY_TASK_TRACK_STARTED = True
CELERY_TASK_TIME_LIMIT = 30 * 60
CELERY_BROKER_URL = env.str("BROKER_URL")
CELERY_RESULT_BACKEND = "django-db"
CELERY_CACHE_BACKEND = "default"
CELERY_ACCEPT_CONTENT = ["json"]
CELERY_TASK_SERIALIZER = "json"
CELERY_BEAT_SCHEDULER = "django_celery_beat.schedulers:DatabaseScheduler"


# app specific settings
CROSSWORDS_LATEST_CLUES_COUNT = env.int("CROSSWORDS_LATEST_CLUES_COUNT", default=20)
CROSSWORDS_RANDOM_CLUES_COUNT = env.int("CROSSWORDS_RANDOM_CLUES_COUNT", default=20)

CROSSWORDS_IMAGE_GENERATOR_FONTS = {
    "light": "fonts/OpenSans/OpenSans-Light.ttf",
    "regular": "fonts/OpenSans/OpenSans-Regular.ttf",
    "medium": "fonts/OpenSans/OpenSans-Medium.ttf",
    "semibold": "fonts/OpenSans/OpenSans-SemiBold.ttf",
    "bold": "fonts/OpenSans/OpenSans-Bold.ttf",
}
