from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path, register_converter

from drf_spectacular.views import (
    SpectacularAPIView,
    SpectacularRedocView,
    SpectacularSwaggerView,
)
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from core.converters import HexUUIDConverter

register_converter(HexUUIDConverter, "hexuuid")

schema_view = SpectacularAPIView.as_view()
swagger_view = SpectacularSwaggerView.as_view(url_name="schema")
redoc_view = SpectacularRedocView.as_view(url_name="schema")

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api-auth/", include("rest_framework.urls")),
    path("token-auth/", obtain_auth_token),
    path("health/", include("health_check.urls")),
    path("", include("pages.urls")),
    path("", include("users.urls")),
    path("", include("clues.urls")),
    path("blog/", include("blog.urls")),
    path("auth/tokens/", TokenObtainPairView.as_view(), name="jwt_obtain_pair"),
    path("auth/tokens/refresh/", TokenRefreshView.as_view(), name="jwt_refresh_pair"),
    path("schema/", schema_view, name="schema"),
    path("schema/swagger-ui/", swagger_view, name="swagger_ui"),
    path("schema/redoc/", redoc_view, name="redoc"),
    path("api/v1/", include("apiv1.urls")),
]

if settings.DEBUG:
    urlpatterns += (
        [
            # type: ignore
            path("__debug__/", include("debug_toolbar.urls")),
        ]
        + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
        + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    )
