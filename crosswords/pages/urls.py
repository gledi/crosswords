from django.urls import path

from drf_spectacular.views import (
    SpectacularAPIView,
    SpectacularRedocView,
    SpectacularSwaggerView,
)

from pages import views
from pages.apps import PagesConfig

app_name = PagesConfig.name

urlpatterns = [
    path("", views.HomeView.as_view(), name="home"),
    path("p/about/", views.AboutView.as_view(), name="about"),
    path("p/contact-us/", views.ContactView.as_view(), name="contact_us"),
    path("p/privacy-policy/", views.PrivacyView.as_view(), name="privacy_policy"),
    path(
        "api/schema/",
        SpectacularAPIView.as_view(),
        name="schema",
    ),
    path(
        "api/schema/swagger-ui/",
        SpectacularSwaggerView.as_view(url_name="schema"),
        name="swagger_ui",
    ),
    path(
        "api/schema/redoc/",
        SpectacularRedocView.as_view(url_name="schema"),
        name="redoc",
    ),
]
