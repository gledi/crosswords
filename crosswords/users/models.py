import uuid

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from .managers import UserManager


class User(AbstractUser):
    username = None
    last_name = None

    id = models.UUIDField(
        _("id"),
        primary_key=True,
        default=uuid.uuid4,
        null=False,
        blank=False,
        editable=False,
    )
    email = models.EmailField(_("email address"), unique=True, null=False, blank=False)
    name = models.CharField(_("first name"), max_length=255, null=False, blank=False)
    created = models.DateTimeField(_("created"), editable=False, default=timezone.now)
    modified = models.DateTimeField(_("created"), editable=False, default=timezone.now)

    objects: UserManager = UserManager()

    EMAIL_FIELD = "email"
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["name"]

    class Meta:
        get_latest_by = ("created", "modified")
        verbose_name = _("user")
        verbose_name_plural = _("users")
        db_table = "users"

    def __str__(self):
        return f"{self.email} ({self.get_full_name()})"

    def save(self, *args, **kwargs):
        self.modified = timezone.now()
        return super().save(*args, **kwargs)

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        return self.name

    def get_short_name(self):
        """Return the short name for the user."""
        return self.name
