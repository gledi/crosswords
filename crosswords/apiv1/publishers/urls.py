from django.urls import path

from . import views

urlpatterns = [
    path("", views.publisher_list, name="publisher_list"),
    path("<hexuuid:pk>/", views.publisher_details, name="publisher_details"),
]
