from typing import Any

from django.core.paginator import Paginator

from rest_framework import serializers
from rest_framework.request import Request

from apiv1.serializers import LinkedModelSerializer, UrlModelSerializer
from clues.models import Clue, Crossword, Publisher


class PublisherListSerializer(LinkedModelSerializer):
    class Meta:
        model = Publisher
        fields = (
            "id",
            "name",
            "source_url",
            "is_active",
            "pub_days",
            "publish_in_advance",
            "listing_order",
            "created",
            "modified",
            "links",
        )


class PublisherCreateSerializer(UrlModelSerializer):
    class Meta:
        model = Publisher
        fields = (
            "id",
            "name",
            "source_url",
            "is_active",
            "pub_days",
            "publish_in_advance",
            "listing_order",
            "created",
            "modified",
            "url",
        )
        read_only_fields = ("id", "created", "modified", "url")


class ClueSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clue
        fields = ("id", "clue", "answer", "num", "direction")


class CrosswordSerializer(serializers.ModelSerializer):
    clues = ClueSerializer(many=True)

    class Meta:
        model = Crossword
        fields = ("id", "pubdt", "source", "clues")


class PublisherDetailSerializer(LinkedModelSerializer):
    crosswords = serializers.SerializerMethodField()

    class Meta:
        model = Publisher
        fields = (
            "id",
            "name",
            "source_url",
            "is_active",
            "pub_days",
            "publish_in_advance",
            "listing_order",
            "created",
            "modified",
            "links",
            "crosswords",
        )
        read_only_fields = ("id", "created", "modified", "links", "crosswords")

    def get_crosswords(self, obj: Publisher) -> Any:
        request: Request = self.context["request"]
        page_number = request.query_params.get("page", 1)
        page_size = request.query_params.get("size", 10)
        qs = Crossword.objects.filter(is_published=True).order_by("-pubdt").all()
        paginator = Paginator(qs, per_page=page_size, allow_empty_first_page=True)
        page = paginator.page(page_number)
        serializer = CrosswordSerializer(page.object_list, many=True)
        return serializer.data
