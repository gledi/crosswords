from rest_framework import generics

from clues.models import Publisher

from .serializers import (
    PublisherCreateSerializer,
    PublisherDetailSerializer,
    PublisherListSerializer,
)


class PublisherListCreateView(generics.ListCreateAPIView):
    queryset = Publisher.objects.all()
    serializer_class = PublisherListSerializer

    def get_serializer_class(self):
        if self.request.method == "POST":
            return PublisherCreateSerializer
        return super().get_serializer_class()


publisher_list = PublisherListCreateView.as_view()


class PublisherDetailView(generics.RetrieveAPIView):
    queryset = Publisher.objects.filter(is_active=True).all()
    serializer_class = PublisherDetailSerializer


publisher_details = PublisherDetailView.as_view()
