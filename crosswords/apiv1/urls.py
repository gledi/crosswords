from django.urls import include, path

app_name = "apiv1"

urlpatterns = [path("publishers/", include("apiv1.publishers.urls"))]
