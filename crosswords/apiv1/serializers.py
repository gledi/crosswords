from rest_framework import serializers
from rest_framework.reverse import reverse


class ViewNamesSerializerMixin:
    LIST_VIEW_TEMPLATE = "apiv1:{model_name}_list"
    DETAILS_VIEW_TEMPLATE = "apiv1:{model_name}_details"


class DetailsLinkedModelSerializer(
    ViewNamesSerializerMixin, serializers.ModelSerializer
):
    link = serializers.SerializerMethodField()  # type: ignore

    def get_link(self, obj) -> dict[str, str]:
        model_name = self.Meta.model._meta.model_name  # type: ignore
        view_name = self.DETAILS_VIEW_TEMPLATE.format(model_name=model_name)
        return {
            "rel": "detail",
            "href": reverse(
                view_name,
                kwargs={"pk": obj.pk},
                request=self.context["request"],
            ),
        }


class CollectionLinkedModelSerializer(
    ViewNamesSerializerMixin, serializers.ModelSerializer
):
    link = serializers.SerializerMethodField()  # type: ignore

    def get_link(self, obj) -> dict[str, str]:
        model_name = self.Meta.model._meta.model_name  # type: ignore
        view_name = self.LIST_VIEW_TEMPLATE.format(model_name=model_name)
        return {
            "rel": "collection",
            "href": reverse(view_name, request=self.context["request"]),
        }


class LinkedModelSerializer(ViewNamesSerializerMixin, serializers.ModelSerializer):
    links = serializers.SerializerMethodField()  # type: ignore

    def get_links(self, obj) -> dict[str, str]:
        model_name = self.Meta.model._meta.model_name  # type: ignore
        return {
            "detail": reverse(
                self.DETAILS_VIEW_TEMPLATE.format(model_name=model_name),
                kwargs={"pk": obj.pk},
                request=self.context["request"],
            ),
            "collection": reverse(
                self.LIST_VIEW_TEMPLATE.format(model_name=model_name),
                request=self.context["request"],
            ),
        }


class AltLinkedModelSerializer(ViewNamesSerializerMixin, serializers.ModelSerializer):
    links = serializers.SerializerMethodField()  # type: ignore

    def get_links(self, obj) -> list[dict[str, str]]:
        model_name = self.Meta.model._meta.model_name  # type: ignore
        return [
            {
                "rel": "detail",
                "href": reverse(
                    self.DETAILS_VIEW_TEMPLATE.format(model_name=model_name),
                    kwargs={"pk": obj.pk},
                    request=self.context["request"],
                ),
            },
            {
                "rel": "collection",
                "href": reverse(
                    self.LIST_VIEW_TEMPLATE.format(model_name=model_name),
                    request=self.context["request"],
                ),
            },
        ]


class UrlModelSerializer(ViewNamesSerializerMixin, serializers.ModelSerializer):
    url = serializers.SerializerMethodField()  # type: ignore

    def get_url(self, obj) -> str:
        model_name = self.Meta.model._meta.model_name  # type: ignore
        view_name = self.DETAILS_VIEW_TEMPLATE.format(model_name=model_name)

        return reverse(
            view_name,
            kwargs={"pk": obj.pk},
            request=self.context["request"],
        )
