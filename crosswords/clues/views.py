from __future__ import annotations

import datetime
from typing import Any

from django.contrib.postgres.search import SearchHeadline, SearchQuery, SearchRank
from django.db.models import Count, F, Prefetch, Q, QuerySet
from django.db.models.functions import ExtractMonth, ExtractYear
from django.shortcuts import render
from django.views.generic import DetailView, ListView

from clues.models import Clue, Crossword, Publisher
from clues.utils import MonthlyStat, YearlyStat, get_month_dates


def get_daily_crosswords(request):
    today = datetime.date.today()

    crosswords = (
        Crossword.objects.select_related("publisher")
        .prefetch_related("clues")
        .filter(pubdt=today)
        .all()
    )

    return render(
        request,
        "clues/daily_crosswords.html",
        context={"today": today, "crosswords": crosswords},
    )


class SearchResultsList(ListView):
    model = Clue
    context_object_name = "clues"
    template_name = "clues/search_results.html"
    paginate_by = 20

    def get_queryset(self) -> QuerySet[Clue]:
        q = self.request.GET.get("q")
        if not q:
            return Clue.objects.none()

        search_query = SearchQuery(q)

        return (
            Clue.objects.annotate(rank=SearchRank(F("description"), search_query))
            .annotate(
                headline=SearchHeadline(
                    F("clue"),
                    search_query,
                    start_sel='<span class="hl">',
                    stop_sel="</span>",
                )
            )
            .order_by("-rank")
        )
        # search_vector = SearchVector("clue", weight="A") + SearchVector(
        #     "answer", weight="B"
        # )

        # return (
        #     Clue.objects.annotate(rank=SearchRank(search_vector, search_query))
        #     .filter(rank__gte=0.3)
        #     .order_by("-rank")
        # )

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context_data = super().get_context_data(**kwargs)
        context_data.update({"search_query": self.request.GET.get("q", "")})
        return context_data


class PublisherListView(ListView):
    queryset = Publisher.objects.all()
    context_object_name = "publishers"


class PublisherDetailView(DetailView):
    def get_queryset(self):
        start_date, end_date = get_month_dates()

        return Publisher.objects.prefetch_related(
            Prefetch(
                "crosswords",
                queryset=Crossword.objects.prefetch_related("clues")
                .filter(Q(pubdt__gte=start_date) & Q(pubdt__lte=end_date))
                .order_by("-pubdt")
                .all(),
            )
        ).all()

    def _get_current_year_stats(self, publisher: Publisher | None) -> list[MonthlyStat]:
        start_date, _ = get_month_dates()
        year_start = start_date.replace(month=1, day=1)

        if publisher is None:
            publisher = self.get_object()

        qs = (
            Crossword.objects.filter(publisher=publisher)
            .filter(Q(pubdt__lt=start_date) & Q(pubdt__gte=year_start))
            .annotate(year=ExtractYear("pubdt"), month=ExtractMonth("pubdt"))
            .values("year", "month")
            .annotate(crosswords_count=Count("id"))
            .order_by("-year", "-month")
            .all()
        )
        return [MonthlyStat(**row) for row in qs]

    def _get_previous_years_stats(
        self, publisher: Publisher | None
    ) -> list[YearlyStat]:
        start_date, _ = get_month_dates()
        year_start = start_date.replace(month=1, day=1)

        if publisher is None:
            publisher = self.get_object()

        qs = (
            Crossword.objects.filter(publisher=publisher)
            .filter(pubdt__lt=year_start)
            .annotate(year=ExtractYear("pubdt"))
            .values("year")
            .annotate(crosswords_count=Count("id"))
            .order_by("-year")
            .all()
        )
        return [YearlyStat(**row) for row in qs]

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context_data = super().get_context_data(**kwargs)

        context_data.update(
            {
                "monthly_stats": self._get_current_year_stats(kwargs.get("object")),
                "yearly_stats": self._get_previous_years_stats(kwargs.get("object")),
            }
        )

        return context_data


class CrosswordDetailView(DetailView):
    queryset = Crossword.objects.prefetch_related("clues").all()


class ClueDetailView(DetailView):
    queryset = Clue.objects.select_related("crossword", "crossword__publisher").all()

    def get_context_data(self, **kwargs) -> dict[str, Any]:
        context_data = super().get_context_data(**kwargs)

        clue = kwargs.get("object")
        if clue is None:
            clue = self.get_object()

        context_data.update(
            {
                "clues": clue.crossword.clues.filter(~Q(pk=clue.pk)).all(),
            }
        )
        return context_data
