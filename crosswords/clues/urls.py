from django.urls import path

from clues import views
from clues.apps import CluesConfig

app_name = CluesConfig.name

urlpatterns = [
    path("search/", views.SearchResultsList.as_view(), name="search"),
    path("daily-crosswords/", views.get_daily_crosswords, name="daily_crosswords"),
    path(
        "crosswords/<pk>/",
        views.CrosswordDetailView.as_view(),
        name="crossword_detail",
    ),
    path("publishers/", views.PublisherListView.as_view(), name="publisher_list"),
    path(
        "publishers/<slug>/",
        views.PublisherDetailView.as_view(),
        name="publisher_detail",
    ),
    path("clues/<slug>/", views.ClueDetailView.as_view(), name="clue_detail"),
]
