from typing import Any

from django.contrib.postgres.indexes import GinIndex
from django.contrib.postgres.search import SearchVector, SearchVectorField
from django.db import models
from django.db.models import F
from django.urls import reverse
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _

from taggit.managers import TaggableManager

from core.models import BaseModel


class Publisher(BaseModel):
    name = models.CharField(_("name"), max_length=128, null=False, blank=False)
    slug = models.SlugField(
        _("slug"),
        max_length=128,
        unique=True,
        blank=True,
        editable=False,
    )
    source_url = models.CharField(
        _("source url"),
        null=True,
        blank=True,
        max_length=255,
    )
    is_active = models.BooleanField(_("is active"), default=True)
    pub_days = models.IntegerField(_("publication days"), default=0)
    publish_in_advance = models.SmallIntegerField(_("publish in advance"), default=0)
    listing_order = models.IntegerField(_("listing order"), default=0)

    tags = TaggableManager()

    class Meta:
        db_table = "publishers"
        verbose_name = _("publisher")
        verbose_name_plural = _("publishers")
        get_latest_by = ("created", "modified")

    def __str__(self) -> str:
        return self.name

    def save(self, *args: Any, **kwargs: Any) -> None:
        if not self.slug:
            self.slug = slugify(self.name)
        return super().save(*args, **kwargs)

    def get_absolute_url(self) -> str:
        return reverse("clues:publisher_detail", kwargs={"slug": self.slug})


class Crossword(BaseModel):
    pubdt = models.DateField(_("publication date"))
    source = models.URLField(_("source"), max_length=255, null=True, blank=True)
    publisher = models.ForeignKey(
        Publisher,
        verbose_name=_("publisher"),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="crosswords",
    )
    is_published = models.BooleanField(_("is published?"), default=False)

    tags = TaggableManager()

    class Meta:
        db_table = "crosswords"
        verbose_name = _("crossword")
        verbose_name_plural = _("crosswords")
        get_latest_by = ("created", "modified")

    def __str__(self):
        return f"{self.publisher.name} on {self.pubdt}"  # type: ignore

    def get_absolute_url(self):
        return reverse("clues:publisher_detail", kwargs={"pk": self.pk})


class Clue(BaseModel):
    ACROSS = "a"
    DOWN = "d"
    DIRECTION_CHOICES = [
        (ACROSS, _("Across")),
        (DOWN, _("Down")),
    ]

    clue = models.CharField(_("clue"), max_length=512)
    slug = models.SlugField(_("slug"), max_length=255, blank=True, db_index=True)
    answer = models.CharField(_("answer"), max_length=32)
    picture = models.ImageField(_("picture"), null=True, blank=True, upload_to="clues/")
    num = models.PositiveSmallIntegerField(_("number"), null=True, blank=True)
    direction = models.CharField(
        _("direction"),
        max_length=1,
        null=True,
        blank=True,
        choices=DIRECTION_CHOICES,
    )
    crossword = models.ForeignKey(
        Crossword,
        verbose_name=_("crossword"),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="clues",
    )
    description = SearchVectorField(_("description"), null=True)

    tags = TaggableManager()

    class Meta:
        db_table = "clues"
        verbose_name = _("clue")
        verbose_name_plural = _("clues")
        get_latest_by = ("created", "modified")
        indexes = (GinIndex(fields=("description",)),)

    def __str__(self) -> str:
        return self.clue

    def save(self, *args: Any, **kwargs: Any) -> None:
        if not self.slug:
            self.slug = slugify(self.clue)
        self.description = SearchVector(F("clue"), weight="A") + SearchVector(
            F("answer"), weight="B"
        )
        return super().save(*args, **kwargs)

    def get_absolute_url(self) -> str:
        return reverse("clues:clue_detail", kwargs={"slug": self.slug})
