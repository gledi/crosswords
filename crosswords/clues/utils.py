import calendar
import datetime
from dataclasses import dataclass


def get_month_dates(date=None):
    if date is None:
        date = datetime.date.today()

    _, num_days = calendar.monthrange(date.year, date.month)

    return (date.replace(day=1), date.replace(day=num_days))


@dataclass
class MonthlyStat:
    year: int
    month: int
    crosswords_count: int

    @property
    def date(self) -> datetime.date:
        return datetime.date(self.year, self.month, 1)


@dataclass
class YearlyStat:
    year: int
    crosswords_count: int

    @property
    def date(self) -> datetime.date:
        return datetime.date(self.year, 1, 1)
