from django.contrib import admin

from clues.models import Clue, Crossword, Publisher


class PublisherAdmin(admin.ModelAdmin):
    pass


class CrosswordAdmin(admin.ModelAdmin):
    pass


class ClueAdmin(admin.ModelAdmin):
    pass


admin.site.register(Publisher, PublisherAdmin)
admin.site.register(Crossword, CrosswordAdmin)
admin.site.register(Clue, ClueAdmin)
