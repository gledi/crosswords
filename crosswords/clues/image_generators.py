import io
import random
import re
from pathlib import Path
from typing import Any, Callable

from PIL import Image, ImageDraw, ImageFont

re_answer = re.compile(r"^[A-Z]+$")


class AnswerDimensionError(ValueError):
    pass


class AnswerError(ValueError):
    pass


class RGB:
    White = (255, 255, 255)
    Black = (0, 0, 0)


class AnswerImageGenerator:
    MIN_ROWS = 1
    MIN_COLS = 8
    MAX_ROWS = MAX_COLS = 32
    MIN_CELL_SIZE = 8
    MAX_CELL_SIZE = 64
    MAX_ANSWER_LENGTH = 32

    def __init__(
        self,
        answer: str,
        num: int | None = None,
        columns: int | None = None,
        cell_size: int = 40,
        fonts: dict[str, str] | None = None,
        font_loader: Callable[[str], str] | None = None,
    ):
        if answer is None or answer.strip() == "":
            raise AnswerError("Answer must not be empty")

        self.rows = 3
        self.cols = (
            columns if columns is not None else max(len(answer) + 2, self.MIN_COLS)
        )
        self.cell_size = cell_size

        self.answer = answer
        self.num = num

        self.blanks_per_line = self.cols // 4
        if self.blanks_per_line <= 1:
            self.blanks_per_line += 1

        self.width = self.cols * self.cell_size
        self.height = self.rows * self.cell_size
        self.size = (self.width, self.height)

        self.text_font_size = self.cell_size // 2
        self.text_margin = self.cell_size // 4

        self.num_font_size = self.cell_size // 4
        self.num_margin = self.cell_size // 12

        self._fonts = {} if fonts is None else fonts
        self._font_loader = font_loader

        self._start_pos = (
            0
            if len(self.answer) == self.cols
            else random.randrange(0, self.cols - len(self.answer))
        )
        self._end_pos = self._start_pos + len(self.answer) - 1

        self.image = self._create_image()
        self._imgdraw = ImageDraw.Draw(self.image)
        self._cell_is_single_pixel = True

    @property
    def answer(self) -> str:
        return self._answer

    @answer.setter
    def answer(self, val: str) -> None:
        val = val.upper()
        if not re_answer.match(val):
            raise AnswerError("Only full words can be drawn")

        if len(val) > self.MAX_ANSWER_LENGTH:
            raise AnswerError(
                "Cannot draw answers longer than {self.MAX_ANSWER_LENGTH}"
            )

        if not self.cols or len(val) > self.cols:
            self.cols = len(val) + 2

        self._answer = val

    @property
    def rows(self) -> int:
        return self._rows

    @rows.setter
    def rows(self, val: int) -> None:
        if not (self.MIN_ROWS <= val <= self.MAX_ROWS):
            raise AnswerDimensionError(
                f"Number of rows {val} is not between "
                "{self.MIN_ROWS} and {self.MAX_ROWS}"
            )
        self._rows = val

    @property
    def cols(self) -> int:
        return self._cols

    @cols.setter
    def cols(self, val: int) -> None:
        if not (self.MIN_COLS <= val <= self.MAX_COLS):
            raise AnswerDimensionError(
                f"Number of columns {val} is not between "
                f"{self.MIN_COLS} and {self.MAX_COLS}"
            )
        self._cols = val

    @property
    def cell_size(self) -> int:
        return self._cell_size

    @cell_size.setter
    def cell_size(self, val: int) -> None:
        if not (self.MIN_CELL_SIZE <= val <= self.MAX_CELL_SIZE):
            raise AnswerDimensionError(
                f"Cell size {val} is not between "
                f"{self.MIN_CELL_SIZE} and {self.MAX_CELL_SIZE}"
            )
        self._cell_size = val

    def _create_image(self) -> Image.Image:
        initial_size = (self.cols, self.rows)
        return Image.new("RGB", initial_size, color=RGB.White)

    def _load_font(
        self,
        variant: str = "regular",
        size: int = 12,
    ) -> ImageFont.FreeTypeFont | None:
        font = None
        if self._font_loader is not None and variant in self._fonts:
            relative_path = self._fonts[variant]
            font_path = self._font_loader(relative_path)
            font = ImageFont.truetype(font_path, size=size)
        return font

    def _draw_blank_cells(self) -> None:
        draw = ImageDraw.Draw(self.image)

        # randomly assign blank cells on the top and bottom row
        for row in (0, 2):
            for col in random.sample(
                list(range(self.cols)),
                random.randrange(1, self.blanks_per_line),
            ):
                draw.point((col, row), fill=RGB.Black)

        # if there is a empty cell to the left of text, blank it
        if self._start_pos > 0:
            draw.point((self._start_pos - 1, 1), fill=RGB.Black)

        # if there is a empty cell to the right of text, blank it
        if self._end_pos < (self.cols - 1):
            draw.point((self._end_pos + 1, 1), fill=RGB.Black)

    def _draw_borders(self) -> None:
        draw = ImageDraw.Draw(self.image)

        # vertical borders
        for i in range(self.cell_size, self.width, self.cell_size):
            draw.line([(i, 0), (i, self.height)], fill=RGB.Black, width=1)

        # horizontal borders
        for i in range(self.cell_size, self.height, self.cell_size):
            draw.line([(0, i), (self.width, i)], fill=RGB.Black, width=1)

    def _draw_text(self) -> None:
        draw = ImageDraw.Draw(self.image)
        font = self._load_font("bold", self.text_font_size)
        for i, char in enumerate(self.answer, self._start_pos):
            coords = (
                i * self.cell_size + self.text_margin,
                self.cell_size + self.text_margin,
            )
            draw.text(coords, char, font=font, fill=RGB.Black)

    def _draw_number(self) -> None:
        if self.num is not None:
            draw = ImageDraw.Draw(self.image)
            font = self._load_font("light", size=self.num_font_size)
            coords = (
                self._start_pos * self.cell_size + self.num_margin,
                self.cell_size + self.num_margin,
            )
            draw.text(coords, str(self.num), font=font, fill=RGB.Black)

    def draw(self) -> Image.Image:
        self._draw_blank_cells()
        self.image = self.image.resize(self.size, Image.NEAREST)
        self._cell_is_single_pixel = False
        self._draw_borders()
        self._draw_number()
        self._draw_text()
        return self.image

    def save(self, filename: str | Path | io.BytesIO, **kwargs: Any) -> None:
        self.image.save(filename, format="PNG")


class AnimatedAnswerImageGenerator(AnswerImageGenerator):
    def __init__(
        self,
        answer: str,
        num: int | None = None,
        columns: int | None = None,
        cell_size: int = 40,
        fonts: dict[str, str] | None = None,
        font_loader: Callable[[str], str] | None = None,
    ):
        super().__init__(answer, num, columns, cell_size, fonts, font_loader)
        self._images = []

    def _draw_text(self) -> None:
        self._images.append(self.image.copy())

        font = self._load_font("bold", self.text_font_size)
        for i, char in enumerate(self.answer, self._start_pos):
            draw = ImageDraw.Draw(self.image)
            coords = (
                i * self.cell_size + self.text_margin,
                self.cell_size + self.text_margin,
            )
            draw.text(coords, char, font=font, fill=RGB.Black)
            self._images.append(self.image.copy())

    def save(self, filename: str | Path | io.BytesIO, **kwargs: Any) -> None:
        self._images[0].save(
            filename,
            save_all=True,
            append_images=self._images[1:],
            optimize=kwargs.get("optimize", True),
            duration=kwargs.get("duration", 200),
            loop=kwargs.get("loop", 5),
            format="GIF",
        )
