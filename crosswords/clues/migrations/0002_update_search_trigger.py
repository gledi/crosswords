# Generated by Django 4.0.6 on 2022-07-11 09:26

from django.db import migrations

_sql_create_trigger = """
CREATE TRIGGER trigger_clues_description
BEFORE INSERT OR UPDATE OF clue, answer
ON clues
FOR EACH ROW EXECUTE PROCEDURE
tsvector_update_trigger(
    description, 'pg_catalog.english', clue, answer
);

UPDATE clues SET description = NULL;
"""

_sql_drop_trigger = """
DROP TRIGGER IF EXISTS trigger_clues_description ON clues;
"""


class Migration(migrations.Migration):

    dependencies = [
        ("clues", "0001_initial"),
    ]

    operations = [
        migrations.RunSQL(sql=_sql_create_trigger, reverse_sql=_sql_drop_trigger),
    ]
