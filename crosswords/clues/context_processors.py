from django.conf import settings

from clues.models import Clue, Publisher


def get_publishers(request):
    publishers = (
        Publisher.objects.filter(is_active=True).order_by("listing_order").all()
    )
    return {"active_publishers": publishers}


def get_latest_clues(request):
    ct = settings.CROSSWORDS_LATEST_CLUES_COUNT
    return {
        "latest_clues": Clue.objects.order_by("-created").all()[:ct],
    }


def get_random_clues(request):
    ct = settings.CROSSWORDS_RANDOM_CLUES_COUNT
    clues = Clue.objects.order_by("?")[:ct]
    return {"random_clues": clues}
