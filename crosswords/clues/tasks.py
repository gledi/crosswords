from asgiref.sync import async_to_sync
from celery import shared_task
from celery.utils.log import get_task_logger
from channels.layers import get_channel_layer

logger = get_task_logger("crosswords.clues.tasks")


@shared_task
def send_updates():
    logger.info("Sending app updates ...")
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        "updates",
        {
            "type": "chat.message",
            "message": "from a background task",
        },
    )
    return "OK"
