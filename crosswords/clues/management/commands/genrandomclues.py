import random

from django.core.management.base import BaseCommand
from django.db import transaction
from django.utils.text import slugify

from faker import Faker

from clues.models import Clue, Crossword


class Command(BaseCommand):
    help = "Generate random clues for development purposes"

    def handle(self, *args, **options):
        Faker.seed(42)

        faker = Faker()

        clues = []

        for crossword in Crossword.objects.select_related("publisher").all():
            clues_count = random.randrange(16, 32)
            self.stdout.write(
                f"Generating {clues_count} clues for {crossword.publisher.name} "
                f"crossword published on {crossword.pubdt:%Y-%m-%d}"
            )
            num = 1
            for j in range(clues_count):
                sentence = faker.sentence()[:-1]
                clue = Clue(
                    crossword=crossword,
                    clue=sentence,
                    slug=slugify(sentence),
                    answer=faker.word().upper(),
                    num=num,
                    direction=Clue.DIRECTION_CHOICES[j % 2][0],
                )
                if j % 2 == 0:
                    num += 1
                clues.append(clue)

        with transaction.atomic():
            Clue.objects.bulk_create(clues, batch_size=1000)
            self.stdout.write(f"Generated {len(clues)} clues ...")
