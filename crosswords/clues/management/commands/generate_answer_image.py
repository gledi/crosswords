import io
from typing import Any
from uuid import UUID

from django.conf import settings
from django.contrib.staticfiles.storage import staticfiles_storage
from django.core.files.images import ImageFile
from django.core.management.base import BaseCommand, CommandError, CommandParser

from clues import image_generators
from clues.image_generators import AnswerImageGenerator
from clues.models import Clue


def font_loader(relative_path: str) -> str:
    return staticfiles_storage.path(relative_path)


class Command(BaseCommand):
    help = "Generate image for clue answer"

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument(
            "--animated",
            "-a",
            action="store_true",
            help="Generate animated pictures",
        )

        parser.add_argument("clue_id", type=UUID)

        return super().add_arguments(parser)

    def handle(self, *args: Any, **options: Any) -> str | None:
        try:
            clue = Clue.objects.get(pk=options["clue_id"])
        except Clue.DoesNotExist:
            raise CommandError("Clue does not exist")

        fonts = settings.CROSSWORDS_IMAGE_GENERATOR_FONTS

        prefix = "Animated" if options["animated"] else ""
        cls_name = "{}AnswerImageGenerator".format(prefix)
        ImageGenerator = getattr(image_generators, cls_name, AnswerImageGenerator)
        ext = "gif" if options["animated"] else "png"

        name = "{}-{}.{}".format(clue.key[:5], clue.slug.replace("-", "_"), ext)
        gen = ImageGenerator(
            clue.answer,
            num=clue.num,
            font_loader=font_loader,
            fonts=fonts,
        )
        gen.draw()
        with io.BytesIO() as output:
            gen.save(output)
            output.seek(0)
            clue.picture = ImageFile(output, name=name)
            clue.save()
