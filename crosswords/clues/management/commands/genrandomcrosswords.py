import datetime as dt
import random
from string import Formatter

from django.core.management.base import BaseCommand
from django.db import transaction

from clues.models import Crossword, Publisher


class Command(BaseCommand):
    help = "Generate random crosswords development purposes"

    def handle(self, *args, **options):

        crosswords = []

        publishers = Publisher.objects.all()
        for publisher in publishers:
            self.stdout.write(f"Generating crosswords for {publisher.name}")
            pub_days = [c == "1" for c in bin(publisher.pub_days)[2:].zfill(7)]

            back_in_days = random.randrange(15, 121)
            today = dt.date.today()
            start_date = today + dt.timedelta(days=random.randrange(0, 15))

            for i in range(back_in_days):
                pubdt = start_date - dt.timedelta(days=i)
                self.stdout.write(f"\tCrossword for {pubdt:%Y-%m-%d}")
                if not pub_days[pubdt.weekday()]:
                    self.stdout.write("\tDoesn't publish on this day. Skipping ...")
                    continue

                crossword = Crossword(
                    publisher=publisher,
                    pubdt=pubdt,
                    is_published=pubdt <= today,
                )
                if publisher.source_url:
                    fnames = {
                        fname
                        for _, fname, _, _ in Formatter().parse(publisher.source_url)
                        if fname
                    }
                    fmt_dict = {}

                    if "pubdt" in fnames:
                        fmt_dict["pubdt"] = pubdt

                    if "num" in fnames:
                        fmt_dict["num"] = i + 1
                    crossword.source = publisher.source_url.format(**fmt_dict)

                crosswords.append(crossword)

        with transaction.atomic():
            Crossword.objects.bulk_create(crosswords, batch_size=100)
            self.stdout.write(f"Generated {len(crosswords)} crosswords")
