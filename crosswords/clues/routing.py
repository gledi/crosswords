from django.urls import re_path

from clues import consumers

websocket_urlpatterns = [
    re_path(r"ws/updates/$", consumers.LatestUpdatesConsumer.as_asgi()),  # type: ignore
]
