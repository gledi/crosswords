Crosswords Public Site
======================

Local Development
-----------------

Start the following containers for the db & the cache

```shell
$ docker run --name=crosswords-db -p 5432:5432 -e POSTGRES_USER=crosswords -e POSTGRES_PASSWORD=Cro5sw0rDs -e POSTGRES_DB=crosswordsdb -d postgres:14-alpine
$ docker run --name crosswords-cache -p 6379:6379 -d redis:alpine
```

Example .env file
-----------------

```
ENVIRONMENT=local
DEBUG=1
DJANGO_SETTINGS_MODULE="core.settings.${ENVIRONMENT}"
SECRET_KEY="VerySecretValue"
DATABASE_URL="postgres://crosswords:password@db:5432/crosswordsdb"
CACHE_URL="redis://cache:6379/0"
BROKER_URL="redis://cache:6379/1"
CHANNEL_LAYERS_HOSTS="redis://cache:6379/2"
```

Start the app via docker compose
--------------------------------

Build the necessary images

```shell
$ docker compose build
```

Run the app

```shell
$ docker compose --env-file .env.local up -d
```

Run the app with debug support

```shell
$ docker compose --env-file .env.local --file docker-compose.yml --file docker-compose.debug.yml up -d
```

Exporting requirements
----------------------

```shell
poetry export --without-hashes --without-urls -o requirements/main.txt
poetry export --without-hashes --without-urls --with=test -o requirements/test.txt
poetry export --without-hashes --without-urls --with=prod --with=test --with=dev -o requirements/dev.txt
poetry export --without-hashes --without-urls --with=prod -o requirements/prod.txt
poetry export --without-hashes --without-urls --with=prod -o requirements.txt
```
